import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {

  logout(BuildContext context){
    FirebaseAuth auth = FirebaseAuth.instance;
    auth.signOut().then((what){
      Navigator.pushNamedAndRemoveUntil(context, "/", (Route route)=>false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text("Home sweet Home"),
            RaisedButton(
              onPressed: ()=>logout(context),
              color: Colors.red,
              child: Text(
                "Logout",
                style: TextStyle(
                  color: Colors.white
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
