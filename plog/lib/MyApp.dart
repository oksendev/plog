import 'package:flutter/material.dart';

import 'HomeScreen.dart';
import 'LoginScreen.dart';
import 'Signup.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        "/": (BuildContext context)=>LoginScreen(),
        "/Signup": (BuildContext context)=>Signup(),
        "/Home": (BuildContext context)=>HomeScreen(),
      },
    );
  }
}
