import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Signup extends StatelessWidget {

  TextEditingController emailC = TextEditingController();
  TextEditingController passC = TextEditingController();

  void signup(BuildContext context){
    FirebaseAuth auth = FirebaseAuth.instance;
    if(emailC.text==null || emailC.text.isEmpty) return;
    auth.createUserWithEmailAndPassword(email: emailC.text, password: passC.text).then((FirebaseUser user){
      if(user!=null){
        Navigator.pushNamed(context, "/Home");
      }
    }).catchError((e)=>print(e.toString()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Signup",
              style: TextStyle(fontSize: 50),
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: TextField(
                controller: emailC,
                decoration: InputDecoration(
                    hintText: "Email", prefixIcon: Icon(Icons.email)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: TextField(
                controller: passC,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: "Passowrd",
                  prefixIcon: Icon(Icons.lock),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.green,
                  onPressed: () => signup(context),
                  child: Text(
                    "Signup",
                    style: TextStyle(
                        color: Colors.white
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
