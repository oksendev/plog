import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailC = TextEditingController();

  TextEditingController passC = TextEditingController();

  void login(){
    FirebaseAuth auth = FirebaseAuth.instance;
    if(emailC.text==null || emailC.text.isEmpty) return;
    auth.signInWithEmailAndPassword(email: emailC.text, password: passC.text).then((FirebaseUser user){
      if(user!=null){
        Navigator.pushNamedAndRemoveUntil(context, "/Home", (Route route)=>false);
      }
    }).catchError((e)=>print(e.toString()));
  }

  @override
  void initState() {
    checkUser();
    super.initState();
  }

  checkUser() async{
    FirebaseAuth auth = FirebaseAuth.instance;
    FirebaseUser user = await auth.currentUser();
    if(user != null){
      Navigator.pushNamedAndRemoveUntil(context, "/Home",  (Route route)=>false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Login",
              style: TextStyle(fontSize: 50),
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: TextField(
                controller: emailC,
                decoration: InputDecoration(
                    hintText: "Email", prefixIcon: Icon(Icons.email)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: TextField(
                controller: passC,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: "Passowrd",
                  prefixIcon: Icon(Icons.lock),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.green,
                  onPressed: () => login(),
                  child: Text(
                    "Login",
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: () => Navigator.pushNamed(context, "/Signup"),
                  child: Text("SignUp"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
